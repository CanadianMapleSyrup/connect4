import java.util.Random;
public class SmarterAi extends Player
{
  private Board actualBoard;
  private Board boardCopy;
  private String opponentSymbol;
  private Players players;
  
  private boolean[] fullColumns;
  
  public SmarterAi(String playerSymbol, int playerNum)
  {
    super(playerSymbol, playerNum);
    
    this.fullColumns = new boolean[Board.COLUMNS];
    
  }
  
  
  //Gets the players' info to be able to make a copy of the game board
  public void getPlayerInfo(Players players)
  {
    this.players = players;
    //Making a seperate version of the board for the ai
    this.boardCopy = new Board(this.players);
    this.opponentSymbol = this.players.getAiOpponentSymbol();
  }
  
  //Gets the board from the start of each game
  public void receiveBoard(Board board)
  {
    this.actualBoard = board;
  }

  
  //Gets the Ai's choice
  public int getChoice()
  {
    int choice;
    
    //Choice for itself to win
    choice = getWinningPosition(this.playerSymbol);
    resetBoardCopy();
    if (checkWin(choice,this.playerSymbol))
    {
      System.out.println(choice);
      return choice;
    }
    
    //Choice to stop oppenent from winning
    choice = getWinningPosition(this.opponentSymbol);
    resetBoardCopy();
    if (checkWin(choice,this.opponentSymbol))
    {
      System.out.println(choice);
      return choice;
    }
    
    //Random choice
    Random gen = new Random();
    do
    {
      choice = gen.nextInt(Board.COLUMNS) + 1;
    } while (fullColumns[choice - 1]);
    System.out.println(choice);
    return choice;
  }
  
  //Goes through all positions until it gets a winning position for either player
  private int getWinningPosition(String symbol)
  {
    int notFullColumn = 1;
    for (int j = 1; j <= Board.COLUMNS; j++)
    {
      resetBoardCopy();
      try
      {
        if (checkWin(j,symbol))
        {
          return j;
        }
      }
      catch (ColumnFullException e)
      {
        if (j == notFullColumn)
        {
          this.fullColumns[j -1] = true;
          notFullColumn++;
        }
        continue;
      }
    }
    return notFullColumn;
  }
  
  //Checks if a position is a winning position
  private boolean checkWin(int position, String symbol)
  {
    return boardCopy.playATurnAi(position,symbol);
  }
  
  //Removes all temporary changes to the board that the ai has made
  private void resetBoardCopy()
  {
    this.boardCopy.copyTokenBoard(this.actualBoard);
  }
  
  
}
  
  