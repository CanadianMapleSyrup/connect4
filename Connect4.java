import java.util.Scanner;
import java.util.InputMismatchException;

public class Connect4
{
  private int totalGames;
  private Players players;
  private boolean activateAi;
  private Board board;
  private boolean win;
  
  //Plays a single player's turn
  private void playATurn()
  {
    boolean hasError;
    int column;
    //The loop for player choice error checking
    do
    {
      try
      {
        System.out.print("Player "+this.board.getCurrentPlayerNum()+
                         ". Please enter the column you would like to place your token in (1-"+ Board.COLUMNS+"): ");
        column = this.players.getCurrentChoice();
        this.win = this.board.playATurn(column);
        hasError = false;
      }
      catch (ColumnFullException e)
      {
        System.out.println("Collumn is full");
        hasError = true;
      }
      catch (ArrayIndexOutOfBoundsException e)
      {
        System.out.println("Please enter a number between 1 and "+Board.COLUMNS);
        hasError = true;
      }
      catch (InputMismatchException e)
      {
        System.out.println("Invalid input.");
        hasError = true;
      }
    }while (hasError);
  }
  
  //Plays an entire game
  private void playASingleGame()
  {
    //The loops for a single game.
    do
    {
      playATurn();
    } while (! this.win);
  }
  
  //Does all of the pre-game setup.
  private void preGameSetup()
  {
    //Makes a new board for each game
    this.board = new Board(this.players);
    
    if (this.activateAi)
    {
      this.players.receiveBoardForAi(this.board);
    }
    
    this.totalGames++;
    
    System.out.println(this.board);
    
    this.players.setStartingPlayer(this.totalGames);
  }
  
  //Plays all of the all of the games.
  public void playGames()
  {
    printStartOfProgramMessages();
    this.totalGames = 0;
    //Variable for if the player wants an ai
    this.activateAi = booleanChoiceCheck("Would you like to play against an Ai? (true/false): ");
    this.players = new Players(activateAi);
    boolean again = false;
    if (this.activateAi)
    {
      this.players.getPlayerInfoForAi(this.players);
    }
    do
    {
      preGameSetup();
      playASingleGame();
      printPostGameWinner();
      again = booleanChoiceCheck("Would you like to play again? (true/false): ");
    } while (again);
    printEndOfProgramMessages();
  }
  
  //Prints who won the game or if it was a tie
  private void printPostGameWinner()
  {
    if (this.board.checkTieGame())
    {
      System.out.println("No more playable moves. Tie game!\n");
    }
    else
    {
      System.out.println("Player "+players.getCurrentPlayerNum()+" wins!\n");
      this.players.incrementWins();
    }
  }
  
  //Prints the start of program messages
  private void printStartOfProgramMessages()
  {
    System.out.println("Welcome to Connect4!");
    System.out.println("If you don't know how to play, look it up.\n");
  }
  
  //Prints the end of program messages once the user has decided to stop playing
  private void printEndOfProgramMessages()
  {
    //Final message for when the player plays more than 1 game
    if (this.totalGames > 1)
    {
      if(this.players.checkTiedGame())
      {
        System.out.println("\nTied game!");
      }
      else
      {
        System.out.println("\nPlayer "+this.players.getWinner().getPlayerNum()+" wins with "+this.players.getWins()
                             +" wins out of "+this.totalGames+" games!");
      }
    }
    System.out.println("\nThank you for playing :)");
  }
  
  //Checks if the user enters a valid input for a boolean question
  //Asks the question again if there is an error
  private boolean booleanChoiceCheck(String message)
  {
    Scanner input = new Scanner(System.in);
    boolean hasError = true;
    boolean answer = false;
    do
    {
      try
      {
        System.out.print(message);
        answer = input.nextBoolean();
        hasError = false;
      }
      catch (InputMismatchException e)
      {
        System.out.println("Enter 'true' for yes and 'false' for no");
        hasError = true;
        input = new Scanner(System.in);
      }
    }while (hasError);
    return answer;
  }
  
}