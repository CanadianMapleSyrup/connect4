import java.util.Scanner;

public class Player
{
  //Contains the player's token's symbol
  protected String playerSymbol;
  //Contains how many wins the player has.
  protected int playerWins;
  //Contains the int that represents the player
  protected int playerNum;
  //Contains how many tokens this player has in a row
  protected int playerStreak;
  
  public Player(String playerSymbol, int playerNum)
  {
    this.playerSymbol = playerSymbol;
    this.playerNum = playerNum;
    this.playerWins = 0;
  }
  
  //Gets the player's symbol
  public String getPlayerSymbol()
  {
    return this.playerSymbol;
  }
  
  //Gets the number of wins the player has
  public int getPlayerWins()
  {
    return this.playerWins;
  }
  
  //Increments the number of wins the player has
  public void incrementWins()
  {
    this.playerWins++;
  }
  
  //Gets the number that represents the player
  public int getPlayerNum()
  {
    return this.playerNum;
  }
  
  //Gets the number that represents the player in an array
  public int getIndexNum()
  {
    return this.playerNum - 1;
  }
  
  //Gets the player's streak
  public int getStreak()
  {
    return this.playerStreak;
  }
  
  //Resets the player's streak.
  public void resetStreak()
  {
    this.playerStreak = 1;
  }
  
  //Increments the player's streak
  public void incrementStreak()
  {
    this.playerStreak++;
  }
  
  //Gets the player's choice
  public int getChoice()
  {
    Scanner input = new Scanner(System.in);
    int choice = input.nextInt();
    return choice;
  }
  
  //Next 2 methods are only needed in the SmarterAi
  //Go there for explanation
  public void receiveBoard(Board board)
  {
    
  }
  
  public void getPlayerInfo(Players players)
  {
    
  }
}