public class Token
{
  //Holds the colour and symbol of a player's token
  private String colour;
  public static final String EMPTY_TOKEN = "0";
  
  public Token(String colour)
  {
    this.colour = colour;
  }
  
  public String getColour()
  {
    return this.colour;
  }
  
  public String toString()
  {
    return this.colour;
  }
}