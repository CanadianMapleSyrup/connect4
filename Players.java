public class Players
{
  //The number of playes for this game
  //MUST BE LESS THAN 27 FOR UNIQUE SYMBOLS AND LESS THAN 7 FOR UNIQUE COLOURS
  //MUST BE 2 PLAYERS FOR AI TO WORK
  //MUST BE AT LEAST 2 PLAYERS
  public static final int NUM_PLAYERS = 2;
  
  //Contains the int that represents the current player
  private int currentPlayerNum;
  //Contains the current player
  private Player currentPlayer;
  
  //Array that contains all of the players
  private Player[] players = new Player[Players.NUM_PLAYERS];
  
  //If the ai is active, it will always be player 1
  private static final int AI_INDEX_POSITION = 0;
  
  public Players(boolean activateAi)
  {
    //Symbol for player 1 is X
    String symbol;
    char lastChar = 'X';
    this.currentPlayerNum = 1;
    
    //Sets up each player with a different symbol (each different letter of the alphabet).
    for (int i = 0; i < players.length; i++)
    {
      if (lastChar > 'Z')
      {
        lastChar = 'A';
      }
      //Uses ANSI colour codes.
      //\u001B[31m is red, and it goes to 36m which is cyan.
      //\u001B[0m resets the colours of all following text to the default colour.
      //(i+1-6*(i/6)) makes it so that all the symbols go through the same 6 colours each time.
      symbol = "\u001B[3"+(i+1-6*(i/6))+"m"+lastChar+"\u001B[0m";
      
      //Makes the first player an ai if the user wants to play it.
      if (i == Players.AI_INDEX_POSITION && activateAi)
      {
        players[i] = new SmarterAi(symbol, i + 1);
      }
      else
      {
        players[i] = new Player(symbol, i + 1);
      }
      lastChar++;
    }
  }
  
  //Changes to the next player
  public void changePlayer()
  {
    if (this.currentPlayerNum == Players.NUM_PLAYERS)
    {
      //Goes back to player 1
      this.currentPlayerNum = 1;
      this.currentPlayer = this.players[this.currentPlayerNum - 1];
    }
    else
    {
      this.currentPlayerNum++;
      this.currentPlayer = this.players[this.currentPlayerNum - 1];
    }
  }
  
  
  //Gets the current player's number
  public int getCurrentPlayerNum()
  {
    return this.currentPlayer.getPlayerNum();
  }
  
  //Gets the current player's number for use in arrays
  public int getCurrentPlayerIndex()
  {
    return this.currentPlayer.getIndexNum();
  }
  
  //Gets the current player's symbol
  public String getCurrentPlayerSymbol()
  {
    return this.currentPlayer.getPlayerSymbol();
  }
  
  //Gets the current player's streak
  public int getCurrentStreak()
  {
    return this.currentPlayer.getStreak();
  }
  
  //Resets the current player's streak.
  public void resetCurrentStreak()
  {
    this.currentPlayer.resetStreak();
  }
  
  //Increases the current player's streak
  public void incrementCurrentStreak()
  {
    this.currentPlayer.incrementStreak();
  }
  
  //Gets the number of wins for the player with the most wins
  public int getWins()
  {
    int maxWinnerIndex = getWinner().getIndexNum();
    return this.players[maxWinnerIndex].getPlayerWins();
  }
  
  //Increments the number of wins the current player has
  public void incrementWins()
  {
    this.currentPlayer.incrementWins();
  }
  
  //Returns the player with the most wins.
  public Player getWinner()
  {
    Player winner = this.players[0];
    for (Player player : this.players)
    {
      if (player.getPlayerWins() > winner.getPlayerWins())
      {
        winner = player;
      }
    }
    
    return winner;
  }
  
  //Checks if the scores of all players are tied.
  public boolean checkTiedGame()
  {
    int wins = this.players[0].getPlayerWins();
    
    for (Player player : this.players)
    {
      if (player.getPlayerWins() != wins)
      {
        return false;
      }
    }
    return true;
  }
  
  //Sets the starting player based on how many games have been played
  public void setStartingPlayer(int totalGames)
  {
    //This will reduce total games by NUM_PLAYERS to allow for currentPlayerNum = totalsGames
    //to alternate who starts each game
    while (totalGames > Players.NUM_PLAYERS)
    {
      totalGames -= Players.NUM_PLAYERS;
    }
    this.currentPlayerNum = totalGames;
    this.currentPlayer = this.players[this.currentPlayerNum - 1];
  }
  
  //Gets the current player's choice
  public int getCurrentChoice()
  {
    return this.currentPlayer.getChoice();
  }
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////
  //AI SPECIFIC METHODS
  
  //The ai is always position 0, this gives the board to the ai.
  public void receiveBoardForAi(Board board)
  {
    this.players[Players.AI_INDEX_POSITION].receiveBoard(board);
  }
  
  //Gets the ai's opponent's symbol
  public String getAiOpponentSymbol()
  {
    return this.players[Players.AI_INDEX_POSITION + 1].getPlayerSymbol();
  }
  
  //Gets the players' info so the ai is be able to make a copy of the game board
  public void getPlayerInfoForAi(Players players)
  {
    this.players[Players.AI_INDEX_POSITION].getPlayerInfo(players);
  }
  
  //Changes the current player to the ai
  //currentPlayerNum starts at 1
  public void changeToAi()
  {
    this.currentPlayerNum = Players.AI_INDEX_POSITION + 1;
    this.currentPlayer = this.players[this.currentPlayerNum - 1];
  }
  
  //Changes the current player to the ai's opponent
  //The ai's opponent is always 1 after it
  public void changeToAiOpponent()
  {
    this.currentPlayerNum = Players.AI_INDEX_POSITION + 2;
    this.currentPlayer = this.players[this.currentPlayerNum - 1];
  }
}