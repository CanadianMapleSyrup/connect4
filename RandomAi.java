import java.util.Random;

public class RandomAi extends Player
{
  
  public RandomAi(String playerSymbol, int playerNum)
  {
    super(playerSymbol, playerNum);
  }
  
  //Gets the Ai's random choice
  public int getChoice()
  {
    Random gen = new Random();
    int choice = gen.nextInt(Board.COLUMNS) + 1;
    return choice;
  }
}