public class Board
{
  //Board dimensions
  //Printed board starts to look weird with more than 9 columns
  public static final int COLUMNS = 7;
  public static final int ROWS = 6;
  
  //The board itself
  private Token[][] board;
  
  //How many tokens in a row a player needs to win
  public static final int WINNING_NUM = 4;
  
  //All the players playing the game
  private Players players;
  
  //These contain the position of the most recently placed token
  private int mostRecentTokenRow = 0;
  private int mostRecentTokenColumn = 0;
  
  //The minimum amount of tokens required in a single direction starting from a specified token
  //to qualify the player for a win
  public static final int TOKENS_AROUND = Board.WINNING_NUM - 1;
  
  //Determines whether the token checks go left or right.
  //RIGHT = 1 so it checks to the right of an array index
  public static final int RIGHT = 1;
  //LEFT = -1 so it checks to the left of an array index
  public static final int LEFT = -1;
  
  public Board(Players players)
  {
    //Makes and fills the board with tokens
    this.board = new Token[Board.ROWS][Board.COLUMNS];
    for (int i = 0; i < this.board.length; i++)
    {
      for (int j = 0; j < this.board[i].length; j++)
      {
        this.board[i][j] = new Token(Token.EMPTY_TOKEN);
      }
    }
    this.players = players;
  }
  
  //Updates the board to include most recent token placement
  private void updateBoard(int column)
  {
    //User inputs an interger > 0, so in order to work with arrays, subtract 1
    column--;
    
    //Checks for valid column number
    if (column < 0 || column >= this.board[0].length)
    {
      throw new ArrayIndexOutOfBoundsException("Please enter a valid column number 1-"+this.board[0].length+".");
    }
    //Checks if the column is full
    else if (this.board[0][column].getColour() != Token.EMPTY_TOKEN)
    {
      throw new ColumnFullException();
    }
    
    placeToken(column);
  }
  
  //Places the current player's token into the specified column
  private void placeToken(int column)
  {
    //Replacing an empty token in the specified column
    for (int i = this.board.length - 1; i >= 0; i--)
    {
      if (this.board[i][column].getColour() == Token.EMPTY_TOKEN)
      {
        this.board[i][column] = new Token(this.players.getCurrentPlayerSymbol());
        this.mostRecentTokenRow = i;
        this.mostRecentTokenColumn = column;
        break;
      }
    }
  }
  
  //Check if the win condition has been met (4 tokens of the same colour in a row)
  public boolean checkWin()
  {
    if (checkHorizontalWin(Board.LEFT) || checkHorizontalWin(Board.RIGHT))
    {
      return true;
    }
    this.players.resetCurrentStreak();
    if (checkVerticalWin())
    {
      return true;
    }
    this.players.resetCurrentStreak();
    if (checkDiagonalWin(Board.RIGHT))
    {
      return true;
    }
    this.players.resetCurrentStreak();
    if (checkDiagonalWin(Board.LEFT))
    {
      return true;
    }
    if (checkTieGame())
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  //Checks if the top row is full to determine if there is a tie game
  public boolean checkTieGame()
  {
    for (int j = 0; j < this.board[0].length; j++)
    {
      if (this.board[0][j].getColour() == Token.EMPTY_TOKEN)
      {
        return false;
      }
    }
    return true;
  }
  
  //Helper method for all check[Direction]Win methods
  //If a player has tokens in a row, this method adds to their streak
  //If the streak is interupted, the loop that calls it should break
  private boolean modifyStreak(int i, int j)
  {
    boolean shouldBreak = false;
    if (this.board[i][j].getColour() == this.players.getCurrentPlayerSymbol())
    {
      this.players.incrementCurrentStreak();
      return shouldBreak;
    }
    else
    {
      shouldBreak = true;
      return shouldBreak;
    }
  }
  
  
  //Checks if a player has the winning number of tokens in a row
  private boolean checkWinningStreak()
  {
    if (this.players.getCurrentStreak() >= Board.WINNING_NUM)
    {
      return true;
    }
    else 
    {
      return false;
    }
  }
  
  //Helper method for all check[Direction]Win methods
  //Checks if a column is in bounds
  private boolean checkColumnInBounds(int column)
  {
    if (column < this.board[this.mostRecentTokenRow].length && column >= 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  //Helper method for all check[Direction]Win methods
  //Checks if a row is in bounds
  private boolean checkRowInBounds(int row)
  {
    if (row < this.board.length && row >= 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  //Checks if the current player has 4 of their tokens in a horizontal row
  private boolean checkHorizontalWin(int direction)
  {
    for (int j = 1; j <= Board.TOKENS_AROUND; j++)
    {
      int nextTokenColumn = this.mostRecentTokenColumn + j * direction;
      if (checkColumnInBounds(nextTokenColumn))
      {
        if (modifyStreak(this.mostRecentTokenRow,nextTokenColumn))
        {
          break;
        }
      }
      else
      {
        break;
      }
    }
    return checkWinningStreak();
  }
  
  
  //Checks if either player has 4 of their tokens in a vertical row
  private boolean checkVerticalWin()
  {
    
    for (int i = 1; i <= Board.TOKENS_AROUND; i++)
    {
      int nextTokenRow = this.mostRecentTokenRow + i;
      if (checkRowInBounds(nextTokenRow))
      {
        if (modifyStreak(nextTokenRow,this.mostRecentTokenColumn))
        {
          break;
        }
      }
      else
      {
        break;
      }
    }
    return checkWinningStreak();
    
  }
  
  //Helper method for checkDiagonalWin method
  //Finds how many tokens are in a row in either an upper left or upper right direction
  private void checkUpperDiagonal(int direction)
  {
    
    for (int k = 1; k <= Board.TOKENS_AROUND; k++)
    {
      int nextTokenColumn = this.mostRecentTokenColumn + k * direction;
      int nextTokenRow = this.mostRecentTokenRow - k;
      if (checkRowInBounds(nextTokenRow) && checkColumnInBounds(nextTokenColumn))
      {
        if (modifyStreak(nextTokenRow,nextTokenColumn))
        {
          break;
        }
      }
      else
      {
        break;
      }
    }
  }
  
  //Helper method for checkDiagonalWin method
  //Finds how many tokens are in a row in either a lower left or lower right direction
  private void checkLowerDiagonal(int direction)
  {
    
    for (int k = 1; k <= Board.TOKENS_AROUND; k++)
    {
      int nextTokenColumn = this.mostRecentTokenColumn + k * direction;
      int nextTokenRow = this.mostRecentTokenRow + k;
      if (checkRowInBounds(nextTokenRow) && checkColumnInBounds(nextTokenColumn))
      {
        if (modifyStreak(nextTokenRow,nextTokenColumn))
        {
          break;
        }
      }
      else
      {
        break;
      }
    }
  }
  
  //Checks if a player has 4 of their tokens in a diagonal row
  //direction determines whether it is a left or right diagonal
  private boolean checkDiagonalWin(int direction)
  {
    checkUpperDiagonal(direction);
    int oppositeDirection = direction * -1;
    checkLowerDiagonal(oppositeDirection);
    
    return checkWinningStreak();
  }
  
  //Returns a string of the board with each collum separated by | to allow 
  //for visual representation of the board
  public String toString()
  {
    String visualBoard = getHeader();
    
    for (int i = 0; i < board.length; i++)
    {
      for (int j = 0; j < board[i].length; j++)
      {
        if (j == 0)
        {
          visualBoard += "|";
        }
        visualBoard += board[i][j] + "|";
      }
      visualBoard += "\n";
    }
    visualBoard += getFooter();
    return visualBoard;
  }
  
  //Returns the column numbers for the top of the board
  private String getHeader()
  {
    String header = "\n";
    for (int i = 0; i < board[0].length; i++)
    {
      header += " " + (i + 1);
    }
    
    return header + "\n";
  }
  
  //Return the overscores that form the bottom of the board
  private String getFooter()
  {
    String footer = "�";
    
    for (int i = 0; i < board[0].length; i++)
    {
      footer += "��";
    }
    
    return footer + "\n";
  }
  
  //Plays a single turn
  public boolean playATurn(int column)
  {
    //This allows the result of checkWin to return after the current player
    //has been changed for the next turn
    boolean win;
    
    this.players.resetCurrentStreak();
    
    updateBoard(column);
    
    System.out.println(toString());
    
    win = checkWin();
    
    //Only changes turns if the current player is not the winner
    if (!win)
    {
      this.players.changePlayer();
    }
    
    return win;
  }
  
  //Gets the current player's number to print who's turn it is
  public int getCurrentPlayerNum()
  {
    return this.players.getCurrentPlayerNum();
  }
  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////
  //AI SPECIFIC METHODS
  
  
  
  //Copies the Token board of another board
  //Allows for the ai to have its own copy of the board
  public void copyTokenBoard(Board board)
  {
    
    Token[][] tokenBoard = board.board;
    Token[][] copyTokenBoard = new Token[Board.ROWS][Board.COLUMNS];
    
    for (int i = 0; i < this.board.length; i++)
    {
      for (int j = 0; j < this.board[i].length; j++)
      {
        copyTokenBoard[i][j] = tokenBoard[i][j];
      }
    }
    
    this.board = copyTokenBoard;
    
  }
  
  //Gets the ai's opponent's symbol
  public String getAiOpponentSymbol()
  {
    return this.players.getAiOpponentSymbol();
  }
  
  //Plays a possible turn for the ai
  public boolean playATurnAi(int column, String symbol)
  {
    if (getAiOpponentSymbol() == symbol)
    {
      this.players.changeToAiOpponent();
    }
    else
    {
      this.players.changeToAi();
    }
    
    this.players.resetCurrentStreak();
    
    updateBoard(column,symbol);
    
    boolean win = checkWin();
    
    this.players.changeToAi();
      
    return win;
  }
  
  //Updates the board to include most recent token placement
  //Allows for choice of which player token
  public void updateBoard(int column, String symbol)
  {
    //User inputs an interger > 0, so in order to work with arrays, subtract 1
    column--;
    
    //Checks for valid column number
    if (column < 0 || column >= this.board[0].length)
    {
      throw new ArrayIndexOutOfBoundsException("Please enter a valid column number 1-"+this.board[0].length+".");
    }
    //Checks if the column is free
    else if (this.board[0][column].getColour() != Token.EMPTY_TOKEN)
    {
      throw new ColumnFullException();
    }
    
    placeToken(column,symbol);
  }
  
  //Places a specified token into the specified column
  private void placeToken(int column, String symbol)
  {
    //Replacing an empty token in the specified column
    for (int i = this.board.length - 1; i >= 0; i--)
    {
      if (this.board[i][column].getColour() == Token.EMPTY_TOKEN)
      {
        this.board[i][column] = new Token(symbol);
        this.mostRecentTokenRow = i;
        this.mostRecentTokenColumn = column;
        break;
      }
    }
  }
}